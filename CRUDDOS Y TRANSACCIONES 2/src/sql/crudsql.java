
package sql;

import getset.variables;
import java.sql.Connection;
import javax.swing.JOptionPane;
import java.sql.ResultSet;

public class crudsql extends conexionsql {
    
    java.sql.Statement st;
    ResultSet rs;
    variables var=new variables(); 
    
    public void insertar(String nombre, String apellido, String CI) {
        try {
            Connection conexion = conectar();
            st = conexion.createStatement();
            String sql = "insert into cliente(client_nombre,client_apellido,client_ci) values('" + nombre + "','" + apellido + "','" + CI + "');";
            st.execute(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "El registro se guardo correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "El registro no se guardo " + e, "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void transaccion1(String areaid, String empleadoid, String clienteid, String fecha_r,String horaini,String horafin,String horaalq) {
        try {
            Connection conexion = conectar();
            st = conexion.createStatement();
            String sql = "call transaccion_reserva (" + areaid + "," + empleadoid + "," + clienteid + ",'" + fecha_r + "'::date, '" + horaini  + "'::time, '" + horafin  + "'::time," + horaalq + ");" ;
            st.execute(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "El registro se guardo correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "El registro no se guardo " + e, "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void transaccion2(String area_id, String area_cap, String area_descripcion, String valor_hora) {
        try {
            Connection conexion = conectar();
            st = conexion.createStatement();
            String sql = "call transaccion_areaevento (" + area_id + "," + area_cap + ", '" + area_descripcion + "' ," + valor_hora +  ");" ;
            st.execute(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "El registro se actualizo correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "El registro no se actualizo " + e, "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    
    
    public void mostrar(String client_id) {
        try {
            Connection conexion = conectar();
            st = conexion.createStatement();
            String sql = "select * from cliente where client_id='" + client_id + "';";
            rs = st.executeQuery(sql);
            if (rs.next()) {
                var.setClient_id(rs.getString("client_id"));
                var.setClient_nombre(rs.getString("client_nombre"));
                var.setClient_apellido(rs.getString("client_apellido"));
                var.setClient_ci(rs.getString("client_ci"));
            } else {
                var.setClient_id("");
                var.setClient_nombre("");
                var.setClient_apellido("");
                var.setClient_ci("");
                JOptionPane.showMessageDialog(null, "no se encontro registro", "sin registro", JOptionPane.INFORMATION_MESSAGE);
            }
            st.close();
            conexion.close();

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "error en programa " + e, "Erro de sistema", JOptionPane.ERROR_MESSAGE);
        }
    }
   
    public void actualizar(String nombre, String apellido, String CI, String client_id) {
        try {
            Connection conexion = conectar();
            st = conexion.createStatement();
            String sql = "update cliente set client_nombre='" + nombre + "',client_apellido='" + apellido + "',client_ci='" + CI + "' where client_id='" + client_id + "'; ";
            st.executeUpdate(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "El registro se actualizo", "Exito", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al actualizar " + e, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void eliminar(String client_id){
        try {
            Connection conexion=conectar();
            st=conexion.createStatement();
            String sql="delete from cliente where client_id='"+client_id+"'; ";
            st.executeUpdate(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "Registro eliminado correctamente","Eliminado",JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al eliminar registro "+ e,"Error",JOptionPane.ERROR_MESSAGE);
        }
    }
}
