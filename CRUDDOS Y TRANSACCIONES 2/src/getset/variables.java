
package getset;

    

public class variables {
    
    private static String client_id;
    private static String client_nombre;
    private static String client_apellido;
    private static String client_ci;
    
    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public String getClient_nombre() {
        return client_nombre;
    }

    public void setClient_nombre(String client_nombre) {
        this.client_nombre = client_nombre;
    }

    public String getClient_apellido() {
        return client_apellido;
    }

    public void setClient_apellido(String client_apellido) {
        this.client_apellido = client_apellido;
    }

    public String getClient_ci() {
        return client_ci;
    }

    public void setClient_ci(String client_ci) {
        this.client_ci = client_ci;
    }
    
}
