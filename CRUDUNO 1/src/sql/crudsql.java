
package sql;

import getset.variables;
import java.sql.Connection;
import javax.swing.JOptionPane;
import java.sql.ResultSet;

public class crudsql extends conexionsql {
    
    java.sql.Statement st;
    ResultSet rs;
    variables var=new variables(); 
    
    public void insertar(String fecha, String total) {
        try {
            Connection conexion = conectar();
            st = conexion.createStatement();
            String sql = "insert into factura(fac_fecha,fac_total) values('" + fecha + "','" + total + "');";
            st.execute(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "El registro se guardo correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "El registro no se guardo " + e, "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void mostrar(String fac_id) {
        try {
            Connection conexion = conectar();
            st = conexion.createStatement();
            String sql = "select * from factura where fac_id='" + fac_id + "';";
            rs = st.executeQuery(sql);
            if (rs.next()) {
                var.setClient_id(rs.getString("fac_id"));
                var.setClient_nombre(rs.getString("fac_fecha"));
                var.setClient_apellido(rs.getString("fac_total"));
            } else {
                var.setClient_id("");
                var.setClient_nombre("");
                var.setClient_apellido("");
                var.setClient_ci("");
                JOptionPane.showMessageDialog(null, "no se encontro registro", "sin registro", JOptionPane.INFORMATION_MESSAGE);
            }
            st.close();
            conexion.close();

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "error en programa " + e, "Erro de sistema", JOptionPane.ERROR_MESSAGE);
        }
    }
   
    public void actualizar(String nombre, String apellido, String client_id) {
        try {
            Connection conexion = conectar();
            st = conexion.createStatement();
            String sql = "update factura set fac_fecha='" + nombre + "',fac_total='" + apellido + "' where fac_id='" + client_id + "'; ";
            st.executeUpdate(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "El registro se actualizo", "Exito", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al actualizar " + e, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void eliminar(String client_id){
        try {
            Connection conexion=conectar();
            st=conexion.createStatement();
            String sql="delete from factura where fac_id='"+client_id+"'; ";
            st.executeUpdate(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "Registro eliminado correctamente","Eliminado",JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al eliminar registro "+ e,"Error",JOptionPane.ERROR_MESSAGE);
        }
    }
}
